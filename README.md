# Image Gallery App
Show a large list of images to the user. Something akin to Prexels without the search.

The list shows only thumbnails, user can open individual images to display the full size (or what fits the screen).

Uses the list of images provided by [JSONplaceholder](http://jsonplaceholder.typicode.com)

Images: [http://jsonplaceholder.typicode.com/photos](http://jsonplaceholder.typicode.com/photos)

## Test it
In [Heroku](https://fathomless-tor-26714.herokuapp.com/)

## Features
Infinite scroll that loads 25 images at a time (50 at start).

Mobile version using progressive web app. Open with Chrome and add to home screen to use.

Docker container for deployment to Heroku.

## Deployment

Pull the git repo

Run the development version (without service workers).
``` bash
yarn
yarn start
# navigate to localhost:3000
```

Or run the release version
``` bash
docker build -t image-viewer .
docker run -p 2000:80 --rm image-viewer
# navigate to localhost:2000
```

## Tech
Bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Using react-redux for state management, though the app is small enough to do without.

Using material-ui for styling, rather too complex of a tool for such a small project.

Custom infinite scroll component for the image list.

Modal window for displaying invidual images.

## Tested
Chromium, Firefox, Android Chrome PWA

Loading all images using the infinite scroll (scrolling to the bottom), works.

CPU usage minimal while not loading, high while loading new images.

Memory usage in Chromium at max 300 Mbytes while loading, normally around 200 Mbytes while idle.

## Rejected

### react-infinite-scroll-component

Compared to a custome solution it offers little, it has the same issues as our method.
But also it makes too many calls to load more items which just exarbates issues of having a huge
buffer of images to load.

### online check
navigator.onLine doesn't work for checking online status react-detect-offline works but only
has components not functions: which makes for messy code.

react-detect-offline seems to be the cause for Android PWA crashing after loading a few albums.
Removed for now, needs more investigation.

### image placeholders
using a placeholder for images still loading, looks silly and allows us to scroll past them
to load more placeholders... gets old pretty fast when you have scrolled past the currently loading
images half an hour ago.

### ordering images
Fixing the issue of images jumping because of random load order by saving the load order.
Then rendering using that order.

Causes a lot of instability if done in React, due to the component getting repeat renderings.
The app crashes when scrolling down.

Would require using something else than \<img> to load the image to a temporary location first.

Similar issues if we track the amount of images loaded using counters.

### background loading
For the moment rejected

## Issues
- Loading order is random, we load one album at a time and display images as soon as they are available.
  Multiple http requests get responses in random order
- Can load multiple albums at the same time by scrolling to the page end repeatedly.
  Causes delays and even more extensive issues with random load order.
- Opening an image while thumbnails are loading is slow due to existing http requests.
- No offline indicators in the UI.
- Memory usage is through the roof.

