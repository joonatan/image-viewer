/*  Joonatan Kuosa
 *  2019-12-19
 *
 *  Image Viewer
 */

const reducer = (state = [], action) => {
  switch (action.type) {
    case 'INIT_IMAGES':
      return [...action.data.images]
    default:
      return [...state]
  }
}

export const initImages = () => async (dispatch) => {
  fetch('https://jsonplaceholder.typicode.com/photos')
    .then((res) => res.json())
    .then((json) => {
      // change the flash message
      dispatch({
        type: 'SET_FLASH',
        data: { msg: 'Loading images' },
      })
      // forward data
      dispatch({
        type: 'INIT_IMAGES',
        data: { images: json },
      })
    })
}

export default reducer
