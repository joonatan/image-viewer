/*  Joonatan Kuosa
 *  2019-12-19
 *
 *  Image Viewer
 */

const reducer = (state = { msg: '', look: 'info' }, action) => {
  switch (action.type) {
    case 'SET_FLASH':
      return action.data
    default:
      return state
  }
}

export const setFlash = (text, look = 'info', time = 3) => (dispatch) => {
  setTimeout(() => {
    dispatch({ type: 'SET_FLASH', data: { msg: '', look } })
  }, time * 1000)

  dispatch({
    type: 'SET_FLASH',
    data: {
      msg: text,
      look,
    },
  })
}

export default reducer
