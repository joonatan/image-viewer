/*  Joonatan Kuosa
 *  2019-12-19
 *
 *  Image Viewer
 */

const reducer = (state = { src: '', title: '' }, action) => {
  switch (action.type) {
    case 'OPEN_IMAGE':
      return action.data
    case 'CLOSE_IMAGE':
      return { src: '', title: '' }
    default:
      return state
  }
}

export const closeImage = () => (dispatch) => {
  dispatch({
    type: 'CLOSE_IMAGE',
  })
}

export const openImage = (src_, name_) => (dispatch) => {
  dispatch({
    type: 'OPEN_IMAGE',
    data: { src: src_, title: name_ },
  })
}

export default reducer
