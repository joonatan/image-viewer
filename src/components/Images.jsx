/*  Joonatan Kuosa
 *  2019-12-19
 *
 *  Image Viewer
 */
import React, { useState, useMemo } from 'react'
import PropTypes from 'prop-types'
import * as _ from 'lodash'
import { connect } from 'react-redux'

import Image from './Image'

import { openImage } from '../reducers/modalReducer'
import './Images.css'

// Performance: this has a solid 30% CPU usage while loading
//  memory usage stays the same
//  While not loading: 4-10% CPU
// Same as the plugin we tested
const Scroller = ({ items, openModal }) => {
  // 2 albums is enough for full-hd screen, could be less for mobile
  const [album, setAlbum] = useState(2)

  const maxAlbums = useMemo(() => (
    items.length !== 0 ? items[items.length - 1].albumId : 0
  ), [items])

  const images = useMemo(() => items.filter((i) => i.albumId <= album), [items, album])

  // Binds our scroll event handler
  window.onscroll = _.debounce(() => {
    if (album === maxAlbums) {
      return
    }

    // Checks that the page has scrolled to the bottom
    if (
      window.innerHeight + document.documentElement.scrollTop
        === document.documentElement.offsetHeight
    ) {
      setAlbum(album + 1)
    }
  }, 100)

  return (
    <div>
      {
        images.map((i) => (
          <button key={i.id} type="submit" className="invisible-button" onClick={() => openModal(i.url, i.title)}>
            <Image src={i.thumbnailUrl} />
          </button>
        ))
      }
      { album === maxAlbums && <h3>No more images</h3> }
    </div>
  )
}

const Images = ({ images, openImage }) => (
  <Scroller items={images} openModal={openImage} />
)

Images.propTypes = {
  images: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    albumId: PropTypes.number.isRequired,
    thumbnailUrl: PropTypes.string.isRequired,
  })).isRequired,
}

const mapStateToProps = (state) => (
  {
    images: state.images,
  }
)

export default connect(
  mapStateToProps,
  { openImage },
)(Images)
