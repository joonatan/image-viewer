/*  Joonatan Kuosa
 *  2019-12-19
 *
 *  Flash component
 */
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import clsx from 'clsx'

import SnackbarContent from '@material-ui/core/SnackbarContent'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

import WarningIcon from '@material-ui/icons/Warning'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import ErrorIcon from '@material-ui/icons/Error'
import InfoIcon from '@material-ui/icons/Info'

import { makeStyles } from '@material-ui/core/styles'

import { setFlash } from '../reducers/flashReducer'

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
}

const useStyles = makeStyles((theme) => ({
  success: {
    backgroundColor: 'green',
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.secondary,
  },
  warning: {
    backgroundColor: 'orange',
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
}))

const Flash = ({ msg, look }) => {
  const [open, setOpen] = React.useState(true)

  look = look ? look : 'info'

  const Icon = variantIcon[look]

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }

    setOpen(false)
  }

  const classes = useStyles()

  if (!msg || !open) { return null }

  return (
    <SnackbarContent
      className={clsx(classes[look])}
      open={open}
      onClose={handleClose}
      aria-describedby="message-id"
      message={(
        <span id="message-id" className={classes.message}>
          <Icon className={clsx(classes.icon, classes.iconVariant)} />
          {msg}
        </span>
      )}
      action={[
        <IconButton
          key="close"
          aria-label="close"
          color="inherit"
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>,
      ]}
    />
  )
}

Flash.defaultProps = {
  look: 'info',
}

Flash.propTypes = {
  msg: PropTypes.string.isRequired,
  look: PropTypes.oneOf(['error', 'info', 'success', 'warning']),
}

const mapStateToProps = (state) => ({
  msg: state.flash.msg,
  look: state.flash.look,
})

export default connect(
  mapStateToProps,
  { setFlash },
)(Flash)
