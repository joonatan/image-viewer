/*  Joonatan Kuosa
 *  2019-12-29
 *
 *  Image Viewer
 */
import React from 'react'
import PropTypes from 'prop-types'

const imgStyle = {
  position: 'relative',
  padding: '2px',
  // required for mobile, horizontal and vertical
  maxWidth: '100%',
  maxHeight: '50vh',
  // center the image
  display: 'block',
  marginLeft: 'auto',
  marginRight: 'auto',
}

// Show offline to remove the alt attribute. For displaying lists off images while offline.
const Image = ({ src, alt }) => (
  <div>
    <img style={imgStyle} src={src} alt={alt} />
  </div>
)

Image.defaultProps = {
  alt: '',
}

Image.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
}

export default Image
