/*  Joonatan Kuosa
 *  2019-12-31
 *
 *  Image Viewer
 */
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { Modal, IconButton, makeStyles } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'

import Image from './Image'

import { closeImage } from '../reducers/modalReducer'

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    backgroundColor: '#dddddd',
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    // middle of the screen
    maxWidth: '90vw',
    maxHeight: '90vh',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
}))

const ImageModal = ({ closeImage, modal }) => {
  const classes = useStyles()

  return (
    <Modal
      aria-labelledby="image-modal-title"
      aria-describedby="image-modal-description"
      open={modal.src !== ''}
      onClose={() => closeImage()}
    >
      <div className={classes.paper}>
        <Image id="image-modal-description" src={modal.src} alt={modal.title} />
        <h2 id="image-modal-title">{modal.title}</h2>
        <IconButton
          key="close"
          aria-label="close"
          color="inherit"
          onClick={() => closeImage()}
          style={{ float: 'right' }}
        >
          <CloseIcon />
        </IconButton>
      </div>
    </Modal>
  )
}

ImageModal.propTypes = {
  closeImage: PropTypes.func.isRequired,
  modal: PropTypes.shape({
    src: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }).isRequired,
}

const mapStateToProps = (state) => (
  {
    modal: state.modal,
  }
)

export default connect(
  mapStateToProps,
  { closeImage },
)(ImageModal)
