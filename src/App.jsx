/*  Joonatan Kuosa
 *  2019-12-19
 *
 *  Image Viewer
 */
import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Container, Grid, AppBar, Toolbar, Typography, makeStyles } from '@material-ui/core'

import './App.css'

import Flash from './components/Flash'
import Images from './components/Images'
import ImageModal from './components/ImageModal'

import { setFlash } from './reducers/flashReducer'
import { initImages } from './reducers/imagesReducer'

const gitLink = 'https://gitlab.com/joonatan/image-viewer'

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(10),
  },
  offline: {
    color: 'red',
    margin: '0px 30px',
  },
  status: {
    color: theme.palette.text.secondary,
    margin: '0px 30px',
  },
  footer: {
    paddingBottom: 20,
  },
  footer_text: {
    fontSize: 22,
  },
}))

function App({ initImages, setFlash }) {
  useEffect(() => {
    setFlash('Retrieving images.')
    initImages()
  }, [])

  const classes = useStyles()

  const Footer = () => (
    <Container className={classes.footer}>
      <em className={classes.footer_text}>Imageviewer</em>
      <br />
      <em className={classes.footer_text}>Joonatan Kuosa </em>
      <br />
      <a href={gitLink} target="_blank" rel="noopener noreferrer" className={classes.footer_text}>sources</a>
    </Container>
  )

  return (
    <div className="App">
      <AppBar position="fixed">
        <Toolbar>
          <Grid container direction="row" justify="space-between" alignItems="center">
            <Typography variant="h6">
              Image Viewer
            </Typography>
          </Grid>
        </Toolbar>
        <Flash />
      </AppBar>
      <ImageModal />
      <div className={classes.container}>
        <Images />
      </div>
      <Footer />
    </div>
  )
}

const mapStateToProps = (state) => ({
})

const funcmap = {
  initImages,
  setFlash,
}

export default connect(
  mapStateToProps, funcmap,
)(App)
