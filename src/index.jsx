/*  Joonatan Kuosa
 *  2019-12-19
 *
 *  Image Viewer
 */
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'

import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import * as serviceWorker from './serviceWorker'
import App from './App'

import flashReducer from './reducers/flashReducer'
import imagesReducer from './reducers/imagesReducer'
import modalReducer from './reducers/modalReducer'

const reducer = combineReducers({
  images: imagesReducer,
  modal: modalReducer,
  flash: flashReducer,
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  reducer,
  composeEnhancers(
    compose(applyMiddleware(thunk)),
  ),
)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
)

serviceWorker.register()
