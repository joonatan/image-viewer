FROM node:12.13.1-alpine as build
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN npm install --silent
RUN npm install react-scripts@3.0.1 -g --silent

# Copy build files
COPY src/ /app/src
COPY public/ /app/public

RUN npm run build

# Make a smaller image for release
FROM node:12.13.1-alpine
WORKDIR /app
COPY --from=build /app/build /app/
COPY --from=build /app/node_modules /app/node_modules
ENV PATH /app/node_modules/.bin:$PATH

# Need serve or something to serve the app
RUN npm install -g serve

# TODO if need be install serve and run serve /app
EXPOSE 80
ENV PORT 80

CMD ["serve", "/app"]
