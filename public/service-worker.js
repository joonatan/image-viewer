/*  Joonatan Kuosa
 *  2019-12-29
 *
 *  Image Viewer
 */
const CACHE_NAME = 'image-viewer-cache-v1'
const urlsToCache = [
  '/',
  '/static/css',
  '/static/js',
  'https://jsonplaceholder.typicode.com/photos',
  'https://via.placeholder.com/150',
  'https://via.placeholder.com/600',
]

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then((cache) => {
        // return cache.addAll(urlsToCache)
        cache.addAll(urlsToCache.map((url) => new Request(url, { mode: 'no-cors' })))
          .then(() => {
            console.log('Install: All resources have been fetched and cached.')
          })
      }),
  )
})

self.addEventListener('fetch', (event) => {
  // Don't cache HEAD requests
  if (event.request.method === 'GET') {
    event.respondWith(
      caches.match(event.request)
        .then((response) => {
          // Cache hit - return response
          if (response) {
            return response
          }

          // Network
          return fetch(event.request).then((resp) => {
            // Check if we received a valid response
            if (!resp || resp.status !== 200) {
              return resp
            }

            // IMPORTANT: Clone the response.
            // A response is a stream so both cache and browser consume it.
            const responseToCache = resp.clone()

            caches.open(CACHE_NAME)
              .then((cache) => {
                cache.put(event.request, responseToCache)
              })

            return resp
          }).catch(() => (
            // filter out erros since we are going to get them constantly when offline
            null
          ))
        }).catch(() => (null)),
    )
  } else if (event.request.method === 'HEAD') {
    return fetch(event.request)
      .catch(() => (
        // skip failed HEAD requests because the offline plugin makes these constantly
        null
      ))
  }
})

self.addEventListener('activate', (event) => {
  const cacheWhitelist = [CACHE_NAME]

  event.waitUntil(
    caches.keys().then((cacheNames) => (
      Promise.all(
        cacheNames.map((cacheName) => (
          (cacheWhitelist.indexOf(cacheName) === -1) ? caches.delete(cacheName) : undefined
        )),
      )
    )).catch((err) => (
      console.error('activate error: ', err)
    )),
  )
})
